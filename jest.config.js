module.exports = {
    globals: {
        "ts-jest": {
            tsconfig: "tsconfig.json",
        },
    },
    setupFiles: ["./jest.setup.ts"],
    moduleFileExtensions: ["ts", "js", "json"],
    moduleNameMapper: {
        // Application paths
        "^#application$": "<rootDir>/src/application",
        "^#application/(.*)$": "<rootDir>/src/application/$1",

        // Domain paths
        "^#domain$": "<rootDir>/src/domain",
        "^#domain/(.*)$": "<rootDir>/src/domain/$1",

        // Receivers paths
        "^#receivers$": "<rootDir>/src/receivers",
        "^#receivers/(.*)$": "<rootDir>/src/receivers/$1",
    },
    transform: {
        "^.+\\.ts$": "ts-jest",
    },
    testRegex: "./test/.*.ts",
    testEnvironment: "node",
    collectCoverageFrom: ["./src/**/*.ts"],
    coverageReporters: ["text", "cobertura"],
};
