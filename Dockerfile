FROM bitnami/node:16.17.0 AS build
COPY package.json yarn.lock ./
RUN yarn install
COPY . .
RUN yarn build-minimal

FROM bitnami/node:16.17.0
WORKDIR /app

COPY --from=build /app/node_modules /app/node_modules
COPY --from=build /app/dist /app/dist
COPY package.json ./
COPY tsconfig.json ./
COPY bootstrap ./bootstrap

# Create a non-root user
RUN useradd -r -u 1001 -g root -b /app nonroot && \
    chown -R nonroot /app

USER nonroot

EXPOSE 3000
CMD ["yarn", "start"]
