# Changelog

All notable changes to this project will be documented in this file.

## [Unreleased]

### Changed

-   Update TypeScript to v4.7.2
-   Replace reflect-metadata with shared reflection library (from the core module)

## [1.1.1] - 2022-10-04

## Changed

-   Update Node.js to 16.17.0 ([code/general#418](https://gitlab.com/operator-ict/golemio/code/general/-/issues/418))
-   Change message key for DPP metro to `exchangeName.vehiclepositionsruns.saveMetroRunsToDB` ([pid#166](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/166))

### Removed

-   Unused dependencies ([modules/general#5](https://gitlab.com/operator-ict/golemio/code/modules/general/-/issues/5))

## [1.1.0] - 2022-09-01

### Added

-   Add UDP receiver for DPP metro runs ([pid#165](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/165))

### Changed

-   Refactor existing TCP DPP receivers

## [1.0.3] - 2022-05-04

### Added

-   Factory for TCP DPP receivers
-   Base TCP DPP receiver duplication for trams and busses
-   Prepare base UDP receiver
-   AMQP alternate exchange
