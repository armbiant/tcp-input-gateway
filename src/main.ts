/* istanbul ignore file */
import "@golemio/core/dist/shared/_global";
import { container as commonContainer } from "tsyringe";
import { ApplicationToken, ITransportInputGatewayApp, TransportInputGatewayApp } from "#application";
import { DomainToken, ILoggerService, Config, LoggerService, AMQPService, StorageService } from "#domain";
import { ReceiverToken, DPPReceiverFactory, DPPMetroReceiver, DPPReceiverEnum, DPPConfig } from "#receivers";
import { DPPStorageManager } from "#receivers/managers";
import { DPPTramBusParser, DPPMetroParser } from "#receivers/parsers";

const applicationContainer = commonContainer.createChildContainer();

// Domain registrations
commonContainer
    .register(DomainToken.Configuration, { useValue: Config })
    .registerSingleton(DomainToken.LoggerService, LoggerService)
    .registerSingleton(DomainToken.AMQPService, AMQPService)
    .registerSingleton(DomainToken.StorageService, StorageService);

// Receiver registrations
applicationContainer
    .register(ReceiverToken.DPPConfiguration, { useValue: DPPConfig })
    .register(ReceiverToken.DPPStorageManager, DPPStorageManager)
    .registerSingleton(ReceiverToken.DPPTramBusParser, DPPTramBusParser)
    .registerSingleton(ReceiverToken.DPPMetroParser, DPPMetroParser)
    .registerSingleton(ReceiverToken.Receiver, DPPReceiverFactory.getClass(DPPReceiverEnum.Tram))
    .registerSingleton(ReceiverToken.Receiver, DPPReceiverFactory.getClass(DPPReceiverEnum.Bus))
    .registerSingleton(ReceiverToken.Receiver, DPPMetroReceiver);

// Application registrations
applicationContainer.registerSingleton(ApplicationToken.TransportInputGatewayApp, TransportInputGatewayApp);

// Resolve application instance and start the app
const app = applicationContainer.resolve<ITransportInputGatewayApp>(ApplicationToken.TransportInputGatewayApp);
app.start();

// Handle process signals
process.on("SIGTERM", () => app.stop());
process.on("SIGINT", () => app.stop());

for (const event of ["uncaughtException", "unhandledRejection"]) {
    const log = commonContainer.resolve<ILoggerService>(DomainToken.LoggerService).getLogger();

    process.on(event as NodeJS.Signals, (err) => {
        // Log error, gracefully shut down and exit with exit code 4
        log.error(err);
        app.stop(4);
    });
}
