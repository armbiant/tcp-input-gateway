export const DomainToken = {
    Configuration: Symbol(),
    LoggerService: Symbol(),
    AMQPService: Symbol(),
    StorageService: Symbol(),
};
