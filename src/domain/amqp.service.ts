import { inject, injectable } from "tsyringe";
import amqplib, { Connection, Channel } from "amqplib";
import AlternateExchangeCreator from "@golemio/core/dist/helpers/data-access/amqp/AlternateExchangeCreator";
import { LogObject } from "#domain";
import { DomainToken } from "./constants";
import { IConfiguration } from "./config";
import { ILoggerService, IAMQPService } from "./interfaces";

@injectable()
export class AMQPService implements IAMQPService {
    private readonly config: IConfiguration["rabbit"];
    private readonly log: LogObject;

    private connection?: Connection;
    private channel?: Channel;
    private numberOfReconnectionAttempts: number;
    private gracefulShutdown: boolean;

    constructor(
        @inject(DomainToken.Configuration) config: IConfiguration,
        @inject(DomainToken.LoggerService) loggerService: ILoggerService
    ) {
        this.config = config.rabbit;
        this.log = loggerService.getLogger();

        this.numberOfReconnectionAttempts = 0;
        this.gracefulShutdown = false;
    }

    /**
     * Connect to RabbitMQ server
     */
    public async connect(isFailover = false) {
        if (this.channel && !isFailover) {
            return;
        }

        if (isFailover && !this.gracefulShutdown) {
            this.log.info(`[AMQP] Reconnect after: ${this.config.reconnectionTimeout / 1000} seconds`);
            await new Promise((done) => setTimeout(done, this.config.reconnectionTimeout));

            this.numberOfReconnectionAttempts++;
            this.log.info(`[AMQP] Number of reconnection attempts: ${this.numberOfReconnectionAttempts}`);
        }

        if (!this.config.connectionString) {
            throw new Error("[AMQP] The ENV variable RABBIT_CONN cannot be undefined");
        }

        try {
            this.connection = await amqplib.connect(this.config.connectionString);
            this.channel = await this.connection.createChannel();

            // Connection listeners
            this.connection.on("error", (err: Error) => {
                this.log.error("[AMQP] Connection error", err);
            });

            this.connection.on("close", async () => {
                this.log.warn("[AMQP] Connection closing");

                if (!this.gracefulShutdown) {
                    await this.connect(true);
                }
            });

            // Channel listeners
            this.channel.on("error", (err) => {
                this.log.error(err);
            });

            this.channel.on("close", () => {
                this.log.warn("[AMQP] Channel closing");
                this.channel = undefined;
                if (!this.gracefulShutdown) {
                    this.connection?.close();
                }
            });

            this.log.info("[AMQP] Connected to Queue");
        } catch (err) {
            this.log.error(err);

            if (
                (err.code === "EHOSTUNREACH" || err.code === "ECONNREFUSED") &&
                this.numberOfReconnectionAttempts < this.config.maxReconnections
            ) {
                await this.connect(true);
            } else {
                throw new Error("[AMQP] Error while creating AMQP Channel");
            }
        }
    }

    /**
     * Disconnect from RabbitMQ server
     */
    public async disconnect() {
        this.log.info("[AMQP] Closing connection");
        this.gracefulShutdown = true;

        try {
            await this.channel?.close();
            await this.connection?.close();
        } catch (err) {
            this.log.error("[AMQP] Disconnect error", err);
        }

        return null;
    }

    /**
     * Send message to custom AMQP exchange
     */
    public async sendMessageToExchange(key: string, msg: string, options: Record<string, any> = {}) {
        try {
            await this.channel!.assertExchange(this.config.exchangeName, "topic", {
                durable: false,
                alternateExchange: AlternateExchangeCreator.getAltExchangeName(this.config.exchangeName),
            });
            this.channel!.publish(this.config.exchangeName, key, Buffer.from(msg), options);
        } catch (err) {
            this.log.error("[AMQP] Cannot send the message to exchange", err);
        }
    }
}
