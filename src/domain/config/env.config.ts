export interface IConfiguration {
    nodeEnv: "development" | "test" | "production";
    logLevel: string;
    rabbit: {
        connectionString: string;
        exchangeName: string;
        maxReconnections: number;
        reconnectionTimeout: number;
    };
    s3: {
        enabled: boolean;
        accessKeyID: string;
        endpoint: string;
        secretAccessKey: string;
        bucketName: string;
        uploadPartSize: number;
        uploadQueueSize: number;
    };
}

export const Config: IConfiguration = {
    nodeEnv: (process.env.NODE_ENV as IConfiguration["nodeEnv"]) ?? "development",
    logLevel: process.env.LOG_LEVEL ?? "INFO",
    rabbit: {
        connectionString: process.env.RABBIT_CONN ?? "",
        exchangeName: process.env.RABBIT_EXCHANGE_NAME ?? "",
        maxReconnections: process.env.RABBIT_MAX_RECONNECTIONS ? parseInt(process.env.RABBIT_MAX_RECONNECTIONS) : 5,
        reconnectionTimeout: process.env.RABBIT_RECONNECTION_TIMEOUT ? parseInt(process.env.RABBIT_RECONNECTION_TIMEOUT) : 5000,
    },
    s3: {
        accessKeyID: process.env.S3_ACCESS_KEY_ID ?? "",
        bucketName: process.env.S3_BUCKET_NAME ?? "",
        enabled: process.env.S3_ENABLED === "true",
        endpoint: process.env.S3_ENDPOINT ?? "",
        secretAccessKey: process.env.S3_SECRET_ACCESS_KEY ?? "",
        uploadPartSize: process.env.S3_UPLOAD_PART_SIZE ? parseInt(process.env.S3_UPLOAD_PART_SIZE) : 6,
        uploadQueueSize: process.env.S3_UPLOAD_QUEUE_SIZE ? parseInt(process.env.S3_UPLOAD_QUEUE_SIZE) : 2,
    },
};
