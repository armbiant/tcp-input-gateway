import { inject, injectable } from "tsyringe";
import { S3 } from "aws-sdk";
import moment from "@golemio/core/dist/shared/moment-timezone";
import { LogObject } from "#domain";
import { DomainToken } from "./constants";
import { IConfiguration } from "./config";
import { ILoggerService, IStorageService } from "./interfaces";

@injectable()
export class StorageService implements IStorageService {
    private readonly config: IConfiguration;
    private readonly log: LogObject;
    private readonly s3: S3;

    constructor(
        @inject(DomainToken.Configuration) config: IConfiguration,
        @inject(DomainToken.LoggerService) loggerService: ILoggerService
    ) {
        this.config = config;
        this.log = loggerService.getLogger();

        this.s3 = new S3({
            accessKeyId: config.s3.accessKeyID,
            endpoint: config.s3.endpoint,
            maxRetries: 15,
            retryDelayOptions: {
                customBackoff: (retryCount, err) => {
                    this.log.error(`[S3] Request retry: ${retryCount}`, err);
                    return 1000;
                },
            },
            s3ForcePathStyle: true,
            secretAccessKey: config.s3.secretAccessKey,
            signatureVersion: "v4",
        });
    }

    /**
     * Save raw data to our storage
     */
    public async saveData(data: string, pathPrefix: string, ext: string = "") {
        if (!this.config.s3.enabled) {
            this.log.silly("[S3] Service is disabled. Skipping");
            return;
        }

        const now = moment().tz("Europe/Prague");
        const fileName = `${pathPrefix}/${now.format("YYYY-MM-DD")}/${now.format("HH_mm_ss.SSS")}`;

        const paramsBodyData: S3.PutObjectRequest = {
            Body: data,
            Bucket: this.config.s3.bucketName,
            Key: `${fileName}${ext}`,
        };

        const options = {
            partSize: this.config.s3.uploadPartSize * 1024 * 1024,
            queueSize: this.config.s3.uploadQueueSize,
        };

        try {
            await this.s3
                .upload(paramsBodyData, options)
                .on("httpUploadProgress", (progress) => {
                    this.log.verbose(
                        `[S3] Raw data S3 upload ` +
                            `\`${this.config.s3.bucketName}/${fileName}: \`${progress.loaded} / ${progress.total}.`
                    );
                })
                .promise();
        } catch (err) {
            this.log.error(`[S3] Saving raw data failed. (${fileName})`, err);
        }
    }
}
