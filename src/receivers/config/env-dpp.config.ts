export interface IDPPConfiguration {
    tcp: ITCPDPPConfiguration;
    udp: IUDPDPPConfiguration;
}

export interface ITCPDPPConfiguration {
    tram: IDPPReceiverConfiguration;
    bus: IDPPReceiverConfiguration;
}

export interface IUDPDPPConfiguration {
    metro: IDPPReceiverConfiguration;
}

export interface IDPPReceiverConfiguration {
    port: string;
    bufferSizeLimit: number;
    storageFileSize: number;
    storagePathPrefix: string;
}

export const DPPConfig: IDPPConfiguration = {
    tcp: {
        tram: {
            port: process.env.DPP_TRAM_RECEIVER_PORT ?? "3000",
            bufferSizeLimit: process.env.DPP_TRAM_BUFFER_SIZE_LIMIT ? parseFloat(process.env.DPP_TRAM_BUFFER_SIZE_LIMIT) : 2,
            storageFileSize: process.env.DPP_TRAM_STORAGE_FILE_SIZE ? parseFloat(process.env.DPP_TRAM_STORAGE_FILE_SIZE) : 1,
            storagePathPrefix: process.env.DPP_TRAM_STORAGE_PATH_PREFIX ?? "tcp-dpp-tram-data",
        },
        bus: {
            port: process.env.DPP_BUS_RECEIVER_PORT ?? "3001",
            bufferSizeLimit: process.env.DPP_BUS_BUFFER_SIZE_LIMIT ? parseFloat(process.env.DPP_BUS_BUFFER_SIZE_LIMIT) : 2,
            storageFileSize: process.env.DPP_BUS_STORAGE_FILE_SIZE ? parseFloat(process.env.DPP_BUS_STORAGE_FILE_SIZE) : 1,
            storagePathPrefix: process.env.DPP_BUS_STORAGE_PATH_PREFIX ?? "tcp-dpp-bus-data",
        },
    },
    udp: {
        metro: {
            port: process.env.DPP_METRO_RECEIVER_PORT ?? "3002",
            bufferSizeLimit: process.env.DPP_METRO_BUFFER_SIZE_LIMIT ? parseFloat(process.env.DPP_METRO_BUFFER_SIZE_LIMIT) : 2,
            storageFileSize: process.env.DPP_METRO_STORAGE_FILE_SIZE ? parseFloat(process.env.DPP_METRO_STORAGE_FILE_SIZE) : 1,
            storagePathPrefix: process.env.DPP_METRO_STORAGE_PATH_PREFIX ?? "udp-dpp-metro-data",
        },
    },
};
