import { RemoteInfo } from "dgram";
import { inject, injectable } from "tsyringe";
import { DomainToken, IConfiguration, ILoggerService, IAMQPService } from "#domain";
import { ReceiverToken, IDPPStoragemanager, IDPPMessageParser, IDPPConfiguration, IDPPReceiverConfiguration } from "#receivers";
import { extend } from "./helpers";
import { AbstractUDPReceiver } from "./udp-abstract.receiver";

@injectable()
export class DPPMetroReceiver extends AbstractUDPReceiver {
    protected readonly amqpService: IAMQPService;
    protected readonly storageManager: IDPPStoragemanager;
    protected readonly messageParser: IDPPMessageParser;
    protected readonly receiverConfig: IDPPReceiverConfiguration;
    protected readonly amqpMessageKey: string;

    constructor(
        @inject(DomainToken.Configuration) config: IConfiguration,
        @inject(DomainToken.LoggerService) loggerService: ILoggerService,
        @inject(DomainToken.AMQPService) amqpService: IAMQPService,
        @inject(ReceiverToken.DPPConfiguration) dppConfig: IDPPConfiguration,
        @inject(ReceiverToken.DPPStorageManager) storageManager: IDPPStoragemanager,
        @inject(ReceiverToken.DPPMetroParser) messageParser: IDPPMessageParser
    ) {
        const receiverConfig = dppConfig.udp.metro;

        super(receiverConfig.port, receiverConfig.bufferSizeLimit, loggerService.getLogger());
        this.amqpService = amqpService;
        this.storageManager = storageManager;
        this.messageParser = messageParser;
        this.receiverConfig = receiverConfig;
        this.amqpMessageKey = `input-transport.${config.rabbit.exchangeName}.vehiclepositionsruns.saveMetroRunsToDB`;
    }

    /**
     * Extend handleSocketMessage from AbstractUDPReceiver
     */
    @extend()
    protected async handleSocketMessage(_rinfo: RemoteInfo, data: Buffer) {
        this.storageManager.bufferAndSaveData(data, this.receiverConfig.storagePathPrefix, this.receiverConfig.storageFileSize);

        const message = data.toString("utf8");
        return this.processMessage(message);
    }

    /**
     * Extend onShutdown from AbstractUDPReceiver
     */
    @extend()
    protected onShutdown() {
        return this.storageManager.saveData(this.receiverConfig.storagePathPrefix);
    }

    /**
     * Parse message and forward it to exchange
     */
    private async processMessage(message: string) {
        const payload = await this.messageParser.parseData(message);
        if (!payload) {
            return;
        }

        this.log.debug(payload);
        return this.amqpService.sendMessageToExchange(this.amqpMessageKey, payload, {
            persistent: true,
            timestamp: Date.now(),
        });
    }
}
