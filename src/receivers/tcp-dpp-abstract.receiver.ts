import { IAMQPService } from "#domain";
import { IDPPStoragemanager, IDPPMessageParser, IDPPReceiverConfiguration, TCPSocket } from "#receivers";
import { MessageBuffer } from "./buffers/message.buffer";
import { extend } from "./helpers";
import { AbstractTCPReceiver } from "./tcp-abstract.receiver";

export abstract class AbstractDPPReceiver extends AbstractTCPReceiver {
    protected abstract readonly amqpService: IAMQPService;
    protected abstract readonly storageManager: IDPPStoragemanager;
    protected abstract readonly messageParser: IDPPMessageParser;
    protected abstract readonly receiverConfig: IDPPReceiverConfiguration;
    protected abstract readonly amqpMessageKey: string;

    /**
     * Extend configureSocket from AbstractTCPReceiver
     */
    @extend()
    protected configureSocket(socket: TCPSocket) {
        socket.readBuffer = new MessageBuffer("</M>");
    }

    /**
     * Extend handleSocketData from AbstractTCPReceiver
     */
    @extend()
    protected async handleSocketData(socket: TCPSocket, data: Buffer) {
        this.storageManager.bufferAndSaveData(data, this.receiverConfig.storagePathPrefix, this.receiverConfig.storageFileSize);

        socket.readBuffer!.push(data);
        if (socket.readBuffer!.isFinished()) {
            const message = socket.readBuffer!.getMessage();
            if (!message) {
                this.log.error(`[${this.constructor.name}] Received empty message: buffer[${data}]`);
                return;
            }

            return this.processMessage(message);
        }
    }

    /**
     * Extend handleSocketTimeout from AbstractTCPReceiver
     */
    @extend()
    protected async handleSocketTimeout(socket: TCPSocket) {
        const message = socket.readBuffer?.buffer;
        if (message) {
            return this.processMessage(message);
        }
    }

    /**
     * Extend onShutdown from AbstractTCPReceiver
     */
    @extend()
    protected onShutdown() {
        return this.storageManager.saveData(this.receiverConfig.storagePathPrefix);
    }

    /**
     * Parse message and forward it to exchange
     */
    private async processMessage(message: string) {
        const payload = await this.messageParser.parseData(message);
        if (!payload) {
            return;
        }

        this.log.debug(payload);
        return this.amqpService.sendMessageToExchange(this.amqpMessageKey, payload, {
            persistent: true,
            timestamp: Date.now(),
        });
    }
}
