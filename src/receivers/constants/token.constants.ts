export const ReceiverToken = {
    Receiver: Symbol(),
    DPPConfiguration: Symbol(),
    DPPStorageManager: Symbol(),
    DPPMetroParser: Symbol(),
    DPPTramBusParser: Symbol(),
};
