import { parseStringPromise as xmlParser } from "xml2js";
import { IDPPMessageParser } from "#receivers/interfaces";

export abstract class DPPAbstractParser implements IDPPMessageParser {
    private static REGEXP_NORMALIZED_DATA = /[\u0000-\u001F\u007F-\u009F]/g;

    abstract parseData(data: string): Promise<string | undefined>;

    protected parseXml(str: string): Promise<object> {
        return xmlParser(str, { explicitArray: false, normalize: true, trim: true });
    }

    protected normalizeData(str: string) {
        return str.replace(DPPAbstractParser.REGEXP_NORMALIZED_DATA, "");
    }
}
