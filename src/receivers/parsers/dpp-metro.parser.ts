import { inject, injectable } from "tsyringe";
import { DomainToken, LogObject, ILoggerService } from "#domain";
import { DPPAbstractParser } from "./dpp-abstract.parser";

@injectable()
export class DPPMetroParser extends DPPAbstractParser {
    /** @example <m linka="A" tm="2022-07-20T13:45:34Z" gvd="GD20a"> */
    private static REGEXP_MESSAGE_OPENING_TAG_XML = /^(<m[^>]+>)/;
    /** @example <vlak csp=" 3" csr=" 3" cv="279" ko="1809" odch="25" /> */
    private static REGEXP_VALID_TRAINS_XML = /(<vlak[^>]+>)/g;

    private readonly log: LogObject;

    constructor(@inject(DomainToken.LoggerService) loggerService: ILoggerService) {
        super();
        this.log = loggerService.getLogger();
    }

    /**
     * Parse xml data to json string
     */
    public async parseData(data: string): Promise<string | undefined> {
        let str = "";
        try {
            str = this.normalizeData(data);
            const msg = await this.parseXml(str);
            if (!msg) {
                this.log.warn(`[${this.constructor.name}] Received empty message: buffer[${Buffer.from(data)}]`);
                return;
            }

            if (Object.keys(msg).toString() !== ["m"].toString()) {
                this.log.warn(`[${this.constructor.name}] Received invalid message`);
                return;
            }

            return JSON.stringify(msg);
        } catch (err) {
            this.log.error(
                `[${this.constructor.name}] Error parsing received data. Attempting recovery from "${data.substring(0, 2000)}"`,
                err
            );
            return this.recoverData(str);
        }
    }

    /**
     * Salvage data from invalid xml
     */
    private async recoverData(str: string): Promise<string | undefined> {
        try {
            const messageOpeningTagXml = str.match(DPPMetroParser.REGEXP_MESSAGE_OPENING_TAG_XML);
            const validTrainsXml = str.match(DPPMetroParser.REGEXP_VALID_TRAINS_XML);
            if (!messageOpeningTagXml || !validTrainsXml) {
                this.log.warn(`[${this.constructor.name}] Could not recover partial data`);
                return;
            }

            const msg = await this.parseXml(`${messageOpeningTagXml[0]}${validTrainsXml.join("")}</m>`);
            if (msg) {
                const result = JSON.stringify(msg);
                this.log.info(`[${this.constructor.name}] Data recovery successful`);
                return result;
            }
        } catch (err) {
            this.log.error(`[${this.constructor.name}] Error recovering received data: ${str}`, err);
        }
    }
}
