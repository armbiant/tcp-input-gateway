import { inject, injectable } from "tsyringe";
import { DomainToken, LogObject, ILoggerService } from "#domain";
import { DPPAbstractParser } from "./dpp-abstract.parser";

@injectable()
export class DPPTramBusParser extends DPPAbstractParser {
    /** @example <V turnus="134/3" line="134" evc="3738" events="O" ... /> */
    private static REGEXP_VALID_CONTENT_XML = /(<V[^>]+>)/g;

    private readonly log: LogObject;

    constructor(@inject(DomainToken.LoggerService) loggerService: ILoggerService) {
        super();
        this.log = loggerService.getLogger();
    }

    /**
     * Parse xml data to json string
     */
    public async parseData(data: string): Promise<string | undefined> {
        let str = "";
        try {
            str = this.normalizeData(data);
            const msg = await this.parseXml(str);
            if (msg) {
                return JSON.stringify(msg);
            }
            this.log.warn(`[${this.constructor.name}] Received empty message: buffer[${Buffer.from(data)}]`);
        } catch (err) {
            this.log.error(
                `[${this.constructor.name}] Error parsing received data. Attempting recovery from "${data.substring(0, 2000)}"`,
                err
            );
            return this.recoverData(str);
        }
    }

    /**
     * Salvage data from invalid xml
     */
    private async recoverData(str: string): Promise<string | undefined> {
        try {
            const validStrXml = str.match(DPPTramBusParser.REGEXP_VALID_CONTENT_XML);
            if (!validStrXml) {
                this.log.warn(`[${this.constructor.name}] Could not recover partial data`);
                return;
            }

            const msg = await this.parseXml(`<M>${validStrXml.join("")}</M>`);
            if (msg) {
                const result = JSON.stringify(msg);
                this.log.info(`[${this.constructor.name}] Data recovery successful`);
                return result;
            }
        } catch (err) {
            this.log.error(`[${this.constructor.name}] Error recovering received data: ${str}`, err);
        }
    }
}
