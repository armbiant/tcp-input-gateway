import { inject, injectable } from "tsyringe";
import { DomainToken, IStorageService } from "#domain";
import { IDPPStoragemanager } from "#receivers/interfaces";

@injectable()
export class DPPStorageManager implements IDPPStoragemanager {
    protected storageDataQueue = "";
    private readonly storageService: IStorageService;

    constructor(@inject(DomainToken.StorageService) storageService: IStorageService) {
        this.storageService = storageService;
    }

    public async bufferAndSaveData(data: Buffer, pathPrefix: string, fileSize: number) {
        this.storageDataQueue += `${data.toString("utf8")}\r\n`;

        if (Buffer.byteLength(this.storageDataQueue, "utf8") >= fileSize * 1024 * 1024) {
            await this.saveData(pathPrefix);

            // Empty the queue
            this.storageDataQueue = "";
        }
    }

    public saveData(pathPrefix: string) {
        return this.storageService.saveData(`<root>\r\n${this.storageDataQueue}</root>`, pathPrefix, ".xml");
    }
}
