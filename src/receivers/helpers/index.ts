/**
 * Property decorator, extend original property (must be function)
 */
export const extend = () => (target: Object, property: string, descriptor: PropertyDescriptor) => {
    const parentClass = Object.getPrototypeOf(target.constructor);
    const method = descriptor.value!;

    if (typeof parentClass.prototype[property] !== "function") {
        throw new Error(`${parentClass.name}.${property} is not a function`);
    }

    descriptor.value = function () {
        parentClass.prototype[property].apply(this, arguments);
        return method.apply(this, arguments);
    };
};
