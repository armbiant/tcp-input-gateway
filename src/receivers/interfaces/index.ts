import net from "net";
import { MessageBuffer } from "#receivers/buffers/message.buffer";

export interface IReceiver {
    startListening(): void;
    stopListening(): Promise<Error | null>;
}

export interface IDPPStoragemanager {
    bufferAndSaveData(data: Buffer, pathPrefix: string, fileSize: number): Promise<void>;
    saveData(pathPrefix: string): Promise<void>;
}

export interface IDPPMessageParser {
    parseData(data: string): Promise<string | undefined>;
}

export class TCPSocket extends net.Socket {
    id?: string;
    readBuffer?: MessageBuffer;
}

export interface IActiveTCPClient {
    socket: TCPSocket;
    lastMessage?: Buffer;
}

export type ActiveTCPClients = Map<string, IActiveTCPClient>;
