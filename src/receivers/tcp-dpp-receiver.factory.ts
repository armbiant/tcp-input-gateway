import { inject, injectable } from "tsyringe";
import { DomainToken, IConfiguration, ILoggerService, IAMQPService } from "#domain";
import { ReceiverToken, IDPPStoragemanager, IDPPMessageParser, IDPPConfiguration, IDPPReceiverConfiguration } from "#receivers";
import { AbstractDPPReceiver } from "./tcp-dpp-abstract.receiver";

export enum DPPReceiverEnum {
    Tram,
    Bus,
}

export class DPPReceiverFactory {
    public static getClass = (receiver: DPPReceiverEnum) => {
        let receiverName: string;

        switch (receiver) {
            case DPPReceiverEnum.Tram:
                receiverName = "DPPTramReceiver";
                break;

            case DPPReceiverEnum.Bus:
                receiverName = "DPPBusReceiver";
        }

        @injectable()
        class DPPReceiver extends AbstractDPPReceiver {
            protected readonly amqpService;
            protected readonly storageManager;
            protected readonly messageParser;
            protected readonly receiverConfig;
            protected readonly amqpMessageKey;

            constructor(
                @inject(DomainToken.Configuration) config: IConfiguration,
                @inject(DomainToken.LoggerService) loggerService: ILoggerService,
                @inject(DomainToken.AMQPService) amqpService: IAMQPService,
                @inject(ReceiverToken.DPPConfiguration) dppConfig: IDPPConfiguration,
                @inject(ReceiverToken.DPPStorageManager) storageManager: IDPPStoragemanager,
                @inject(ReceiverToken.DPPTramBusParser) messageParser: IDPPMessageParser
            ) {
                let receiverConfig: IDPPReceiverConfiguration, workerMethod: string;
                switch (receiver) {
                    case DPPReceiverEnum.Tram:
                        receiverConfig = dppConfig.tcp.tram;
                        workerMethod = "saveTramRunsToDB";
                        break;

                    case DPPReceiverEnum.Bus:
                        receiverConfig = dppConfig.tcp.bus;
                        workerMethod = "saveBusRunsToDB";
                }

                super(receiverConfig.port, receiverConfig.bufferSizeLimit, loggerService.getLogger());
                this.amqpService = amqpService;
                this.storageManager = storageManager;
                this.messageParser = messageParser;
                this.receiverConfig = receiverConfig;
                this.amqpMessageKey = `input-transport.${config.rabbit.exchangeName}.vehiclepositions.${workerMethod}`;
            }
        }

        // Set receiver name
        Object.defineProperty(DPPReceiver, "name", { value: receiverName });
        return DPPReceiver;
    };
}
