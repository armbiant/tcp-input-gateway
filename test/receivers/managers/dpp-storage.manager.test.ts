import { container } from "tsyringe";
import { DomainToken } from "#domain";
import { ReceiverToken } from "#receivers";
import { DPPStorageManager } from "#receivers/managers";

const mocks = {
    saveData: jest.fn(),
    byteLength: jest.fn().mockReturnValue(Infinity),
};

global.Buffer.byteLength = mocks.byteLength;

/**
 * Describe DPPStorageManager
 */
describe("Receiver Managers - DPPStorageManager", () => {
    let manager: DPPStorageManager;

    const createDependencyContainer = () => {
        manager = container
            .registerSingleton(
                DomainToken.StorageService,
                class DummyStorageService {
                    saveData = mocks.saveData;
                }
            )
            .registerSingleton(ReceiverToken.DPPStorageManager, DPPStorageManager)
            .resolve(ReceiverToken.DPPStorageManager);
    };

    beforeEach(() => {
        createDependencyContainer();
    });

    afterEach(() => {
        container.clearInstances();
        jest.clearAllMocks();
    });

    // =============================================================================
    // bufferAndSaveData
    // =============================================================================
    describe("bufferAndSaveData", () => {
        test("bufferAndSaveData should be called with correct params", () => {
            manager.bufferAndSaveData(Buffer.from("test data"), "test-path", 1);

            expect(mocks.byteLength).toHaveBeenCalledWith("test data\r\n", "utf8");
            expect(mocks.saveData).toHaveBeenCalledWith("<root>\r\ntest data\r\n</root>", "test-path", ".xml");
        });
    });
});
