import net from "net";
import { LogObject } from "#domain";
import { ActiveTCPClients, TCPSocket } from "#receivers/interfaces";
import { AbstractTCPReceiver } from "#receivers/tcp-abstract.receiver";

const serverCloseMock = jest.fn((callback: Function) => callback());
jest.mock("net", () => ({
    ...jest.requireActual("net"),
    createServer: jest.fn(() => {
        return {
            on: jest.fn((_event: string, _callback: Function) => null),
            listen: jest.fn(),
            listening: true,
            close: serverCloseMock,
            unref: jest.fn(),
        };
    }),
}));

// This is necessary as the AbstractTCPReceiver class is abstract
class TCPReceiver extends AbstractTCPReceiver {}

/**
 * Describe AbstractTCPReceiver
 */
describe("Receivers - AbstractTCPReceiver", () => {
    const log = {
        info: jest.fn(),
        error: jest.fn(),
    };

    const socket = {
        id: "testId",
        write: jest.fn(),
        destroy: jest.fn(),
        end: jest.fn((callback) => callback()),
        unref: jest.fn(),
        setTimeout: jest.fn(),
        on: jest.fn(),
        remoteAddress: "172.0.0.1",
        remotePort: "30000",
    };

    // =============================================================================
    // constructor
    // =============================================================================
    describe("constructor", () => {
        test("properties should be assigned in the constructor", () => {
            const receiver = new TCPReceiver("3000", 2, log as unknown as LogObject);

            expect(receiver["port"]).toEqual("3000");
            expect(receiver["log"]).toStrictEqual(log);
            expect(receiver["server"]).not.toBeUndefined();
            expect(net.createServer).toHaveBeenCalledTimes(1);
        });
    });

    // =============================================================================
    // startListening
    // =============================================================================
    describe("startListening", () => {
        test("startListening should call listen on Server", () => {
            const receiver = new TCPReceiver("3000", 2, log as unknown as LogObject);
            receiver.startListening();

            expect(receiver["server"].listen).toHaveBeenCalledWith("3000", expect.any(Function));
        });
    });

    // =============================================================================
    // stopListening
    // =============================================================================
    describe("stopListening", () => {
        test("stopListening should close and unref the server", async () => {
            const receiver = new TCPReceiver("3000", 2, log as unknown as LogObject);
            const promise = receiver.stopListening();

            expect(receiver["server"].close).toHaveBeenCalledTimes(1);
            expect(receiver["server"].unref).toHaveBeenCalledTimes(1);
            await expect(promise).resolves.toBeNull();
        });

        test("stopListening should destroy all established connections", async () => {
            const receiver = new TCPReceiver("3000", 2, log as unknown as LogObject);
            (receiver["clients"] as ActiveTCPClients) = new Map().set(socket.id, { socket });
            const promise = receiver.stopListening();

            await expect(promise).resolves.toBeNull();
            expect(socket.destroy).toHaveBeenCalledTimes(1);
            expect(socket.end).toHaveBeenCalledTimes(1);
        });

        test("stopListening should resolve with an error", async () => {
            serverCloseMock.mockImplementationOnce((callback: Function) => callback(new Error()));

            const receiver = new TCPReceiver("3000", 2, log as unknown as LogObject);
            const promise = receiver.stopListening();

            await expect(promise).resolves.toBeInstanceOf(Error);
            expect(log.error).toHaveBeenCalledWith("[TCPReceiver] Error while stopping the server", expect.any(Error));
        });
    });

    // =============================================================================
    // logSocketEvent
    // =============================================================================
    describe("logSocketEvent", () => {
        test("logSocketEvent should call the logger with specific parameters", () => {
            const receiver = new TCPReceiver("3000", 2, log as unknown as LogObject);
            receiver["logSocketEvent"]("info", "test", socket as unknown as TCPSocket);

            expect(log.info).toHaveBeenCalledWith({
                receiver: "TCPReceiver",
                eventMessage: "test",
                remoteAddress: socket.remoteAddress,
                remotePort: socket.remotePort,
                socketId: socket.id,
            });
        });

        test("logSocketEvent should call the logger with specific parameters (optional data)", () => {
            const receiver = new TCPReceiver("3000", 2, log as unknown as LogObject);
            receiver["logSocketEvent"]("info", "test", socket as unknown as TCPSocket, "test");

            expect(log.info).toHaveBeenCalledWith({
                receiver: "TCPReceiver",
                eventMessage: "test",
                remoteAddress: socket.remoteAddress,
                remotePort: socket.remotePort,
                socketId: socket.id,
                data: "test",
            });
        });
    });

    // =============================================================================
    // handleSocketData
    // =============================================================================
    describe("handleSocketData", () => {
        test("handleSocketData should send ack on a socket and log ack info", () => {
            const receiver = new TCPReceiver("3000", 2, log as unknown as LogObject);
            receiver["logSocketEvent"] = jest.fn();
            receiver["handleSocketData"](socket as unknown as TCPSocket, Buffer.from("test"));

            expect(receiver["logSocketEvent"]).toHaveBeenCalledWith("silly", "Data received", socket, "test");
            expect(socket.destroy).toHaveBeenCalled();
        });

        test("handleSocketData should reject data, send nack on a socket and log ack info", () => {
            const receiver = new TCPReceiver("3000", 2, log as unknown as LogObject);
            receiver["logSocketEvent"] = jest.fn();
            receiver["handleSocketData"]({ ...socket, bytesRead: 3 * 1024 * 1024 } as unknown as TCPSocket, Buffer.from("test"));

            expect(receiver["logSocketEvent"]).toHaveBeenCalledWith("error", "Data size limit exceeded", {
                ...socket,
                bytesRead: 3 * 1024 * 1024,
            });
            expect(socket.write).toHaveBeenCalledWith("NACK data rejected\r\n");
        });
    });

    // =============================================================================
    // handleSocketEnd
    // =============================================================================
    describe("handleSocketEnd", () => {
        test("handleSocketEnd should log ack info", () => {
            const receiver = new TCPReceiver("3000", 2, log as unknown as LogObject);
            receiver["logSocketEvent"] = jest.fn();
            receiver["handleSocketEnd"](socket as unknown as TCPSocket);

            expect(receiver["logSocketEvent"]).toHaveBeenCalledWith("info", "Closing connection", socket);
        });
    });

    // =============================================================================
    // handleSocketClose
    // =============================================================================
    describe("handleSocketClose", () => {
        test("handleSocketClose should remove connection from the pool and log ack info", () => {
            const receiver = new TCPReceiver("3000", 2, log as unknown as LogObject);
            (receiver["clients"] as ActiveTCPClients) = new Map().set(socket.id, { socket });
            receiver["logSocketEvent"] = jest.fn();
            receiver["handleSocketClose"](socket as unknown as TCPSocket);

            expect(receiver["clients"].size).toEqual(0);
            expect(receiver["logSocketEvent"]).toHaveBeenCalledWith("info", "Connection closed", socket);
        });

        test("handleSocketClose should NOT remove connection from the pool and log ack info", () => {
            const receiver = new TCPReceiver("3000", 2, log as unknown as LogObject);
            (receiver["clients"] as ActiveTCPClients) = new Map().set("blah", { socket });
            receiver["logSocketEvent"] = jest.fn();
            receiver["handleSocketClose"]({ ...socket, remoteAddress: "69.0.0.42" } as unknown as TCPSocket);

            expect(receiver["clients"].size).toEqual(1);
            expect(receiver["logSocketEvent"]).toHaveBeenCalledWith("info", "Connection closed", {
                ...socket,
                remoteAddress: "69.0.0.42",
            });
        });
    });

    // =============================================================================
    // handleSocketTimeout
    // =============================================================================
    describe("handleSocketTimeout", () => {
        test("handleSocketTimeout should log ack info, send nack on a socket and call a callback", () => {
            const receiver = new TCPReceiver("3000", 2, log as unknown as LogObject);
            (receiver["clients"] as ActiveTCPClients) = new Map().set(socket.id, {
                socket,
                lastMessage: Buffer.from("<m>test</m>"),
            });
            receiver["logSocketEvent"] = jest.fn();
            receiver["handleSocketTimeout"](socket as unknown as TCPSocket);

            expect(receiver["logSocketEvent"]).toHaveBeenCalledWith(
                "warn",
                "Connection timed out",
                socket,
                'last received message: "<m>test</m>"'
            );
            expect(socket.destroy).toHaveBeenCalled();
        });
    });

    // =============================================================================
    // handleSocketError
    // =============================================================================
    describe("handleSocketError", () => {
        test("handleSocketError should log ack info", () => {
            const receiver = new TCPReceiver("3000", 2, log as unknown as LogObject);
            (receiver["clients"] as ActiveTCPClients) = new Map().set(socket.id, {
                socket,
                lastMessage: Buffer.from("<m>test</m>"),
            });
            receiver["logSocketEvent"] = jest.fn();
            receiver["handleSocketError"](socket as unknown as TCPSocket, new Error("Some error"));

            expect(receiver["logSocketEvent"]).toHaveBeenCalledWith(
                "error",
                "Connection failure: Some error",
                socket,
                'last received message: "<m>test</m>"'
            );
        });
    });

    // =============================================================================
    // handleUpcomingConnection
    // =============================================================================
    describe("handleUpcomingConnection", () => {
        test("handleUpcomingConnection should configure a new socket and create event listeners", () => {
            const receiver = new TCPReceiver("3000", 2, log as unknown as LogObject);
            receiver["handleUpcomingConnection"](socket as unknown as TCPSocket);

            expect(socket.setTimeout).toHaveBeenCalledWith(60000);
            expect(socket.on).toHaveBeenCalledTimes(5);
        });
    });

    // =============================================================================
    // getLastClientMessage
    // =============================================================================
    describe("getLastClientMessage", () => {
        test("getLastClientMessage should return a string representing the last socket message", () => {
            const receiver = new TCPReceiver("3000", 2, log as unknown as LogObject);
            (receiver["clients"] as ActiveTCPClients) = new Map().set(socket.id, {
                socket,
                lastMessage: Buffer.from("<m>test</m>"),
            });

            expect(receiver["getLastClientMessage"](socket as unknown as TCPSocket)).toEqual(
                'last received message: "<m>test</m>"'
            );
        });

        test("getLastClientMessage should return undefined", () => {
            const receiver = new TCPReceiver("3000", 2, log as unknown as LogObject);
            expect(receiver["getLastClientMessage"](socket as unknown as TCPSocket)).toBeUndefined();
        });
    });
});
