import { container } from "tsyringe";
import { DomainToken, Config } from "#domain";
import {
    ReceiverToken,
    DPPReceiverEnum,
    DPPReceiverFactory,
    AbstractDPPReceiver,
    IDPPConfiguration,
    IDPPReceiverConfiguration,
} from "#receivers";

const mocks = {
    getLogger: jest.fn().mockReturnValue({}),
};

const tramConfig: IDPPReceiverConfiguration = {
    port: "4242",
    bufferSizeLimit: 2,
    storageFileSize: 1,
    storagePathPrefix: "test-tram",
};

const busConfig: IDPPReceiverConfiguration = {
    port: "6969",
    bufferSizeLimit: 2,
    storageFileSize: 2,
    storagePathPrefix: "test-bus",
};

jest.mock("net", () => ({
    ...jest.requireActual("net"),
    createServer: jest.fn(() => {
        return {
            on: jest.fn((_event: string, _callback: Function) => null),
            listen: jest.fn(),
        };
    }),
}));

/**
 * Describe DPPReceiverFactory
 */
describe("Receivers - DPPReceiverFactory", () => {
    const createDependencyContainer = () => {
        container
            .register(DomainToken.Configuration, {
                useValue: { ...Config, rabbit: { ...Config.rabbit, exchangeName: "test" } },
            })
            .register(ReceiverToken.DPPConfiguration, {
                useValue: {
                    tcp: {
                        tram: tramConfig,
                        bus: busConfig,
                    },
                } as IDPPConfiguration,
            })
            .registerSingleton(
                DomainToken.LoggerService,
                class DummyLoggerService {
                    getLogger = mocks.getLogger;
                }
            )
            .registerSingleton(DomainToken.AMQPService, class DummyAMQPService {})
            .register(
                ReceiverToken.DPPStorageManager,
                class DummyStorageManager {
                    bufferAndSaveData = jest.fn();
                    saveData = jest.fn();
                }
            )
            .registerSingleton(
                ReceiverToken.DPPTramBusParser,
                class DummyParser {
                    parseData = jest.fn();
                }
            );
    };

    beforeEach(() => {
        createDependencyContainer();
    });

    afterEach(() => {
        container.clearInstances();
        jest.clearAllMocks();
    });

    // =============================================================================
    // getClass
    // =============================================================================
    describe("getClass", () => {
        test("getClass should return class for trams", async () => {
            const TramClass = DPPReceiverFactory.getClass(DPPReceiverEnum.Tram);
            expect(TramClass.name).toEqual("DPPTramReceiver");

            const receiver = container
                .registerSingleton("DPPTramReceiver", TramClass)
                .resolve<AbstractDPPReceiver>("DPPTramReceiver");

            expect(receiver["port"]).toEqual(tramConfig.port);
            expect(receiver["receiverConfig"]).toStrictEqual(tramConfig);
            expect(receiver["amqpMessageKey"]).toEqual(`input-transport.test.vehiclepositions.saveTramRunsToDB`);
        });

        test("getClass should return class for busses", async () => {
            const BusClass = DPPReceiverFactory.getClass(DPPReceiverEnum.Bus);
            expect(BusClass.name).toEqual("DPPBusReceiver");

            const receiver = container
                .registerSingleton("DPPBusReceiver", BusClass)
                .resolve<AbstractDPPReceiver>("DPPBusReceiver");
            expect(receiver["port"]).toEqual(busConfig.port);
            expect(receiver["receiverConfig"]).toStrictEqual(busConfig);
            expect(receiver["amqpMessageKey"]).toEqual(`input-transport.test.vehiclepositions.saveBusRunsToDB`);
        });
    });
});
