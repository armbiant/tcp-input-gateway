import { container } from "tsyringe";
import { RemoteInfo } from "dgram";
import { IConfiguration, DomainToken, Config } from "#domain";
import { ReceiverToken, DPPMetroReceiver, IDPPReceiverConfiguration, IDPPConfiguration } from "#receivers";

const log = {
    error: jest.fn(),
    warn: jest.fn(),
    info: jest.fn(),
    debug: jest.fn(),
};

const mocks = {
    sendMessageToExchange: jest.fn(),
    parseData: jest.fn(),
    bufferAndSaveData: jest.fn(),
    saveData: jest.fn(),
    getLogger: jest.fn().mockReturnValue(log),
    byteLength: jest.fn().mockReturnValue(Infinity),
};

jest.mock("dgram", () => ({
    ...jest.requireActual("dgram"),
    createSocket: jest.fn(() => {
        return {
            on: jest.fn(),
            bind: jest.fn(),
            disconnect: jest.fn(),
            close: jest.fn(),
        };
    }),
}));

const metroConfig: IDPPReceiverConfiguration = {
    port: "4242",
    bufferSizeLimit: 2,
    storageFileSize: 2,
    storagePathPrefix: "test-metro",
};

global.Buffer.byteLength = mocks.byteLength;

/**
 * Describe DPPMetroReceiver
 */
describe("Receivers - DPPMetroReceiver", () => {
    let receiver: DPPMetroReceiver;
    const rinfo: Partial<RemoteInfo> = {
        address: "127.0.0.1",
        port: 4200,
    };

    const createDependencyContainer = (rabbitConfig: Partial<IConfiguration["rabbit"]> = {}) => {
        receiver = container
            .createChildContainer()
            .register(DomainToken.Configuration, {
                useValue: { ...Config, rabbit: { ...Config.rabbit, ...rabbitConfig } },
            })
            .register(ReceiverToken.DPPConfiguration, {
                useValue: {
                    udp: {
                        metro: metroConfig,
                    },
                } as IDPPConfiguration,
            })
            .registerSingleton(
                DomainToken.LoggerService,
                class DummyLoggerService {
                    getLogger = mocks.getLogger;
                }
            )
            .registerSingleton(
                DomainToken.AMQPService,
                class DummyAMQPService {
                    sendMessageToExchange = mocks.sendMessageToExchange;
                }
            )
            .register(
                ReceiverToken.DPPStorageManager,
                class DummyStorageManager {
                    bufferAndSaveData = mocks.bufferAndSaveData;
                    saveData = mocks.saveData;
                }
            )
            .registerSingleton(
                ReceiverToken.DPPMetroParser,
                class DummyParser {
                    parseData = mocks.parseData;
                }
            )
            .registerSingleton(ReceiverToken.Receiver, DPPMetroReceiver)
            .resolve(ReceiverToken.Receiver);
    };

    afterEach(() => {
        container.clearInstances();
        jest.clearAllMocks();
    });

    // =============================================================================
    // constructor
    // =============================================================================
    describe("constructor", () => {
        test("properties should be assigned in the constructor", () => {
            createDependencyContainer();
            expect(receiver["port"]).toEqual("4242");
            expect(receiver["log"]).not.toBeUndefined();
            expect(receiver["server"]).not.toBeUndefined();
        });
    });

    // =============================================================================
    // handleSocketMessage
    // =============================================================================
    describe("handleSocketData", () => {
        test("handleSocketMessage should call parent.handleSocketMessage", async () => {
            createDependencyContainer();
            receiver["logSocketEvent"] = jest.fn();
            await receiver["handleSocketMessage"](rinfo as RemoteInfo, Buffer.from("<test>data</test>"));

            expect(receiver["logSocketEvent"]).toHaveBeenCalledWith("silly", "Data received", rinfo, "<test>data</test>");
        });

        test("handleSocketMessage should send message to queue and store it to our storage", async () => {
            createDependencyContainer({ exchangeName: "test" });
            mocks.parseData.mockResolvedValueOnce('{"test":"data"}');
            receiver["logSocketEvent"] = jest.fn();
            jest.spyOn(Date, "now").mockImplementation(() => 1623680638000);
            await receiver["handleSocketMessage"](rinfo as RemoteInfo, Buffer.from("<M>data</M>"));

            expect(mocks.sendMessageToExchange).toHaveBeenCalledTimes(1);
            expect(mocks.sendMessageToExchange).toHaveBeenCalledWith(
                "input-transport.test.vehiclepositionsruns.saveMetroRunsToDB",
                '{"test":"data"}',
                {
                    persistent: true,
                    timestamp: 1623680638000,
                }
            );
            expect(mocks.bufferAndSaveData).toHaveBeenCalledTimes(1);
            expect(mocks.parseData).toHaveBeenCalledTimes(1);
        });
    });

    // =============================================================================
    // onShutdown
    // =============================================================================
    describe("onShutdown", () => {
        test("onShutdown should execute saveData", () => {
            createDependencyContainer();
            receiver["onShutdown"]();

            expect(log.info).toHaveBeenCalledWith("[DPPMetroReceiver] Executing onShutdown");
            expect(mocks.saveData).toHaveBeenCalledWith("test-metro");
        });
    });
});
