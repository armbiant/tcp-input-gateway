import { container } from "tsyringe";
import { DomainToken } from "#domain";
import { ReceiverToken } from "#receivers";
import { DPPTramBusParser } from "#receivers/parsers";

const log = {
    error: jest.fn(),
    warn: jest.fn(),
    info: jest.fn(),
    debug: jest.fn(),
};

const mocks = {
    getLogger: jest.fn().mockReturnValue(log),
};

/**
 * Describe DPPTramBusParser
 */
describe("Receiver Parsers - DPPTramBusParser", () => {
    let parser: DPPTramBusParser;

    const createDependencyContainer = () => {
        parser = container
            .registerSingleton(
                DomainToken.LoggerService,
                class DummyLoggerService {
                    getLogger = mocks.getLogger;
                }
            )
            .registerSingleton(ReceiverToken.DPPTramBusParser, DPPTramBusParser)
            .resolve(ReceiverToken.DPPTramBusParser);
    };

    beforeEach(() => {
        createDependencyContainer();
    });

    afterEach(() => {
        container.clearInstances();
        jest.clearAllMocks();
    });

    // =============================================================================
    // parseData
    // =============================================================================
    describe("parseData", () => {
        test("parseData should parse valid xml", async () => {
            const result = await parser.parseData(
                // eslint-disable-next-line max-len
                '<M><V turnus="134/3" line="134" evc="3738" np="ano" lat="50.03952" lng="14.42919" akt="07960001" takt="2022-06-29T17:28:13" konc="01100005" tjr="2022-06-29T17:23:00" pkt="12709236" tm="2022-06-29T17:28:22" events="O" /></M>'
            );

            expect(result).toEqual(
                // eslint-disable-next-line max-len
                '{"M":{"V":{"$":{"turnus":"134/3","line":"134","evc":"3738","np":"ano","lat":"50.03952","lng":"14.42919","akt":"07960001","takt":"2022-06-29T17:28:13","konc":"01100005","tjr":"2022-06-29T17:23:00","pkt":"12709236","tm":"2022-06-29T17:28:22","events":"O"}}}}'
            );
            expect(log.error).toHaveBeenCalledTimes(0);
        });

        test("parseData should try to recover data if xml is not valid", async () => {
            const input =
                // eslint-disable-next-line max-len
                '<M><V turnus="134/3" line="134" evc="3738" np="ano" lat="50.03952" lng="14.42919" akt="07960001" takt="2022-06-29T17:28:13" konc="01100005" tjr="2022-06-29T17:23:00" pkt="12709236" tm="2022-06-29T17:28:22" events="O" />';
            const result = await parser.parseData(input);

            expect(result).toEqual(
                // eslint-disable-next-line max-len
                '{"M":{"V":{"$":{"turnus":"134/3","line":"134","evc":"3738","np":"ano","lat":"50.03952","lng":"14.42919","akt":"07960001","takt":"2022-06-29T17:28:13","konc":"01100005","tjr":"2022-06-29T17:23:00","pkt":"12709236","tm":"2022-06-29T17:28:22","events":"O"}}}}'
            );
            expect(log.error).toHaveBeenCalledWith(
                `[DPPTramBusParser] Error parsing received data. Attempting recovery from "${input}"`,
                expect.any(Error)
            );
            expect(log.warn).toHaveBeenCalledTimes(0);
        });

        test("parseData should resolve and log error if xml is not valid", async () => {
            const parsePromise = parser.parseData("not xml");

            await expect(parsePromise).resolves.toBeUndefined();
            expect(log.error).toHaveBeenCalledWith(
                '[DPPTramBusParser] Error parsing received data. Attempting recovery from "not xml"',
                expect.any(Error)
            );
            expect(log.warn).toHaveBeenCalledWith("[DPPTramBusParser] Could not recover partial data");
        });
    });
});
