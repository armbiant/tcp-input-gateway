import { container } from "tsyringe";
import { DomainToken } from "#domain";
import { ReceiverToken } from "#receivers";
import { DPPMetroParser } from "#receivers/parsers";

const log = {
    error: jest.fn(),
    warn: jest.fn(),
    info: jest.fn(),
    debug: jest.fn(),
};

const mocks = {
    getLogger: jest.fn().mockReturnValue(log),
};

/**
 * Describe DPPMetroParser
 */
describe("Receiver Parsers - DPPMetroParser", () => {
    let parser: DPPMetroParser;

    const createDependencyContainer = () => {
        parser = container
            .registerSingleton(
                DomainToken.LoggerService,
                class DummyLoggerService {
                    getLogger = mocks.getLogger;
                }
            )
            .registerSingleton(ReceiverToken.DPPMetroParser, DPPMetroParser)
            .resolve(ReceiverToken.DPPMetroParser);
    };

    beforeEach(() => {
        createDependencyContainer();
    });

    afterEach(() => {
        container.clearInstances();
        jest.clearAllMocks();
    });

    // =============================================================================
    // parseData
    // =============================================================================
    describe("parseData", () => {
        test("parseData should parse valid xml", async () => {
            const result = await parser.parseData(
                '<m linka="A" tm="2022-07-20T13:45:34Z" gvd="GD20a"><vlak csp=" 3" csr=" 3" cv="279" ko="1809" odch="25" /></m>'
            );

            expect(result).toEqual(
                // eslint-disable-next-line max-len
                '{"m":{"$":{"linka":"A","tm":"2022-07-20T13:45:34Z","gvd":"GD20a"},"vlak":{"$":{"csp":" 3","csr":" 3","cv":"279","ko":"1809","odch":"25"}}}}'
            );
            expect(log.error).toHaveBeenCalledTimes(0);
        });

        test("parseData should try to recover data if xml is not valid", async () => {
            const input =
                // eslint-disable-next-line max-len
                '<m linka="A" tm="2022-07-20T13:45:34Z" gvd="GD20a"><vlak csp=" 3" csr=" 3" cv="279" ko="1809" odch="25" /><vlak csp';
            const result = await parser.parseData(input);

            expect(result).toEqual(
                // eslint-disable-next-line max-len
                '{"m":{"$":{"linka":"A","tm":"2022-07-20T13:45:34Z","gvd":"GD20a"},"vlak":{"$":{"csp":" 3","csr":" 3","cv":"279","ko":"1809","odch":"25"}}}}'
            );
            expect(log.error).toHaveBeenCalledWith(
                `[DPPMetroParser] Error parsing received data. Attempting recovery from "${input}"`,
                expect.any(Error)
            );
            expect(log.warn).toHaveBeenCalledTimes(0);
        });

        test("parseData should resolve and log error if xml is not valid", async () => {
            const parsePromise = parser.parseData("not xml");

            await expect(parsePromise).resolves.toBeUndefined();
            expect(log.error).toHaveBeenCalledWith(
                '[DPPMetroParser] Error parsing received data. Attempting recovery from "not xml"',
                expect.any(Error)
            );
            expect(log.warn).toHaveBeenCalledWith("[DPPMetroParser] Could not recover partial data");
        });
    });
});
