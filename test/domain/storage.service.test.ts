import { container } from "tsyringe";
import { IConfiguration, DomainToken, Config, IStorageService } from "#domain";
import { StorageService } from "#domain/storage.service";

const log = {
    silly: jest.fn(),
    verbose: jest.fn(),
    error: jest.fn(),
};

const mocks = {
    getLogger: jest.fn().mockReturnValue(log),
};

const S3Instance = {
    upload: jest.fn().mockReturnThis(),
    on: jest.fn(),
};

jest.mock("aws-sdk", () => {
    return { S3: jest.fn(() => S3Instance) };
});

jest.mock("@golemio/core/dist/shared/moment-timezone", () => {
    const formatting = jest
        .fn(() => "default")
        .mockImplementationOnce(() => "2021-06-01")
        .mockImplementationOnce(() => "18_00_00.000");

    return jest.fn(() => {
        return {
            tz: jest.fn(() => ({ format: formatting })),
        };
    });
});

/**
 * Describe StorageService
 */
describe("Domain - StorageService", () => {
    let service: IStorageService;

    const createDependencyContainer = (s3Config: Partial<IConfiguration["s3"]> = {}) => {
        service = container
            .createChildContainer()
            .register(DomainToken.Configuration, {
                useValue: { ...Config, s3: { ...Config.s3, ...s3Config } },
            })
            .registerSingleton(
                DomainToken.LoggerService,
                class DummyLoggerService {
                    getLogger = mocks.getLogger;
                }
            )
            .registerSingleton(DomainToken.StorageService, StorageService)
            .resolve(DomainToken.StorageService);
    };

    afterEach(() => {
        container.clearInstances();
        jest.clearAllMocks();
    });

    // =============================================================================
    // constructor
    // =============================================================================
    describe("constructor", () => {
        test("constructor should call getLogger", () => {
            createDependencyContainer();
            expect(mocks.getLogger).toHaveBeenCalledTimes(1);
        });
    });

    // =============================================================================
    // saveData
    // =============================================================================
    describe("saveData", () => {
        test("saveData should log error if s3 disabled", async () => {
            createDependencyContainer();
            await expect(service.saveData("test data", "testPrefix")).resolves.toBeUndefined();
            expect(log.silly).toHaveBeenCalledWith("[S3] Service is disabled. Skipping");
        });

        test("saveData should save data so s3", async () => {
            createDependencyContainer({
                enabled: true,
                bucketName: "test",
                uploadPartSize: 1,
                uploadQueueSize: 1,
            });
            const s3DataQueue = `
            -- 2021-06-01T18:00:00.000Z --
            test data
            -- END --`;

            await expect(service.saveData(s3DataQueue, "pathPrefix")).resolves.toBeUndefined();
            expect(S3Instance.upload).toHaveBeenCalledWith(
                {
                    Body: s3DataQueue,
                    Bucket: "test",
                    Key: "pathPrefix/2021-06-01/18_00_00.000",
                },
                { partSize: 1048576, queueSize: 1 }
            );
            expect(S3Instance.on).toHaveBeenCalledWith("httpUploadProgress", expect.any(Function));
        });
    });
});
