const net = require("net");
const dgram = require("dgram");
const { setTimeout: sleep } = require("timers/promises");

const [_execPath, _scriptPath, protocol, port, data] = process.argv;

if (protocol === "tcp") {
    const socket = net.createConnection(port, "127.0.0.1");
    socket.on("connect", async () => {
        await sleep(100);
        socket.write(Buffer.from(data), async (err) => {
            if (err) {
                console.error(err);
            }

            await sleep(100);
            socket.end(() => {
                socket.destroy();
            });
        });
    });
} else if (protocol === "udp") {
    const socket = dgram.createSocket({ type: "udp4" });
    socket.connect(port, "127.0.0.1", async () => {
        await sleep(100);
        socket.send(Buffer.from(data), async (err) => {
            if (err) {
                console.error(err);
            }

            await sleep(100);
            socket.disconnect();
            socket.close();
        });
    });
}
